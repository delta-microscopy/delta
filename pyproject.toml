[build-system]
requires = ["setuptools", "oldest-supported-numpy", "setuptools-git-versioning"]
build-backend = "setuptools.build_meta"

[project]
name = "delta2"
dynamic = ["version"]
description = "Segments and tracks bacteria"
readme = "README.md"
requires-python = ">=3.11"
classifiers = [
    "Development Status :: 5 - Production/Stable",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: MIT License",
    "Topic :: Scientific/Engineering :: Bio-Informatics",
    "Topic :: Scientific/Engineering :: Image Processing",
]
keywords = [
    "bacteria",
    "fluorescence",
    "microscopy",
]
authors = [
    { name = "Jean-Baptiste Lugagne", email = "jblugagne@bu.edu" },
    { name = "Owen OConnor", email = "ooconnor@bu.edu" },
    { name = "Virgile Andreani", email = "andreani@bu.edu" },
]
dependencies = [
    "numpy >= 1.25, < 2.0",
    "scipy >= 1.11",
    "pooch >= 1.8",
    "scikit-image >= 0.20",
    "opencv-python-headless >= 4.7",
    "tqdm >= 4.66",
    "ffmpeg-python >= 0.2",
    "xarray >= 2023.04",
    "netcdf4 >= 1.7",
    "aicsimageio[all] >= 4.10",
    "pip >= 24.0",
    "termcolor >= 2.4",
    "keras >= 3.4",
    "pytest >= 7.3",
    "pytest-cov >= 4.1",
    "elasticdeform >= 0.5",
    "pre-commit >= 3.3",
]

[project.scripts]
delta = "delta.cli:main"

[project.optional-dependencies]
docs = [
    "sphinx >= 8.0",
    "numpydoc >= 1.8",
    "docutils >= 0.21.2",
    "matplotlib >= 3.9",
    "sphinx-design >= 0.6",
    "furo >= 2024.8.6",
    "torch >= 2.5",
]
torch-cpu = [
    "torch >= 2.5",
]
torch-gpu = [
    "torch >= 2.5",
]
jax-cpu = [
    "jax >= 0.4",
]
jax-gpu = [
    "jax[cuda12] >= 0.4",
]
tf-cpu = [
    "tensorflow >= 2.17",
]
tf-gpu = [
    "tensorflow[and-cuda] >= 2.17",
]

[project.license]
text = "MIT License"

[project.urls]
repository = "https://gitlab.com/delta-microscopy/delta"
documentation = "https://delta.readthedocs.io"

[tool.setuptools]
packages = ["delta"]

[tool.setuptools-git-versioning]
enabled = true
dev_template = "{tag}+{branch}{ccount}.{sha}"
dirty_template = "{tag}+{branch}{ccount}.{sha}.dirty"

[tool.ruff.lint]
select = ["ALL"]
ignore = [
    # TODOs and stuff
    "FIX",
    # Object does not implement __hash__ method
    "PLW1641",
    # Use of assert detected
    "S101",
    # Private member accessed
    "SLF001",
    # Pandas
    "PD",
    # Magic values in comparisons
    "PLR2004",
    # Copyright
    "CPY",
    # Type annotation for **kwargs
    "ANN003",
    # Too many things
    "PLR09",
    # Too complex
    "C901",
    # Line too long
    "E501",
    # Unnecessary assign
    "RET504",
    # Causes conflicts on dual-boot systems
    "EXE002",
    # Documentation (TODO: to remove)
    "DOC201",
    "DOC501",

    # Rules that don't play well with ruff format
    # Commas
    "COM812",
    # Implicitly concatenated string literals on one line
    "ISC001",
    # Whitespace before punctuation
    "E203",
]
preview = true

[tool.ruff.format]
docstring-code-format = true

[tool.ruff.lint.pydocstyle]
convention = "numpy"

[tool.ruff.lint.flake8-pytest-style]
parametrize-names-type = "csv"

[tool.ruff.lint.per-file-ignores]
"__init__.py" = ["E402", "F401"]
"tests/test_*.py" = ["ANN", "D100", "D101", "D102", "D103", "S101", "PLR2004", "N999", "ERA001"]
"scripts/*.py" = ["INP001", "T201", "ERA001"]
"docs/source/conf.py" = ["INP001", "A001"]

[tool.mypy]
strict = false
warn_unreachable = true
enable_error_code = ["ignore-without-code", "redundant-expr", "truthy-bool"]
packages = ["delta", "scripts"]
python_version = "3.11"
warn_return_any = true
warn_unused_configs = true
ignore_missing_imports = true

[tool.deptry.package_module_name_map]
opencv-python-headless = "cv2"

[tool.deptry.per_rule_ignores]
DEP002 = ["pip", "pytest", "pytest-cov", "pre-commit"]

[tool.pytest.ini_options]
minversion = "7"
testpaths = ["tests"]
log_cli_level = "INFO"
xfail_strict = true
addopts = ["-ra", "--strict-config", "--strict-markers", "-k not test_train"]

[tool.pixi.feature.cuda]
channels = ["nvidia"]
dependencies = { cuda = ">=12" }

[tool.pixi.feature.torch]
activation = { env = { KERAS_BACKEND = "torch" } }

[tool.pixi.feature.jax]
activation = { env = { KERAS_BACKEND = "jax" } }

[tool.pixi.feature.tf]
activation = { env = { KERAS_BACKEND = "tensorflow" } }

[tool.pixi.project]
channels = ["conda-forge"]
platforms = ["linux-64"]

[tool.pixi.pypi-dependencies]
delta2 = { path = ".", editable = true }

[tool.pixi.dependencies]
libxml2 = ">=2.13"
libxslt = ">=1.1"
lxml = ">=4.9, <5"

[tool.pixi.environments]
default = { solve-group = "default" }
docs = { features = ["docs"], solve-group = "default" }
torch-cpu = { features = ["torch-cpu", "torch"], solve-group = "torch" }
torch-gpu = { features = ["cuda", "torch-gpu", "torch"], solve-group = "torch" }
jax-cpu = { features = ["jax-cpu", "jax"], solve-group = "jax" }
jax-gpu = { features = ["jax-gpu", "jax"], solve-group = "jax" }
tf-gpu = { features = ["tf-gpu", "tf"], solve-group = "tf" }
tf-cpu = { features = ["tf-cpu", "tf"], solve-group = "tf" }
