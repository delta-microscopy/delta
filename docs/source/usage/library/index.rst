Library Usage
=============

If using the command-line interface is not enough to analyze your experiment,
you will need to import DeLTA as a library and write a script using its
functions to do exactly what you want with your data.

In order to do this, you will need to understand the :class:`delta.config`
module, the :class:`delta.pipeline` module, the :class:`delta.lineage` module,
and some of the :class:`delta.utils` module.  The next four sections are
high-level overviews of these modules.  We also provide example scripts that
you are free to reuse and adapt to your needs.

.. toctree::
   :hidden:

   config_desc
   pipeline_desc
   lineage_desc
   utils_desc
   scripts
