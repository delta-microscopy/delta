#!/usr/bin/env python
"""
Run the DeLTA pipeline on a demo movie.

@author: jeanbaptiste
"""

from delta.config import Config
from delta.pipeline import Pipeline
from delta.utils import XPReader

# Load config ('2D' or 'mothermachine'):
config = Config.default("2D")

# Use demo movie as example (or replace by a path to your own movie):
file_path = config.demo_movie_path()

# Init reader:
xpreader = XPReader(file_path)

# Init pipeline:
xp = Pipeline(xpreader, config=config)

# Run it (you can specify which positions, which frames to run etc):
xp.process()
